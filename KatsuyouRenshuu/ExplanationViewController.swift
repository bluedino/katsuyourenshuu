//
//  ExplanationViewController.swift
//  KatsuyouRenshuu
//
//  Created by Matthew Burke on 10/25/21.
//  Copyright © 2021 BlueDino. All rights reserved.
//

import UIKit

class ExplanationViewController: UIViewController {
    public static let identifier = "ExplanationViewController"
    public var name: String?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var closeButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let name = name {
            titleLabel.text = name
        }

        if let url = Bundle.main.url(forResource: "conjugation", withExtension: "html"),
           let data = try? String(contentsOf: url, encoding: .utf8).data(using: .utf8) {
            textView.attributedText = try? NSAttributedString(data: data,
                                                         options: [
                                                            .documentType: NSAttributedString.DocumentType.html
                                                         ],
                                                         documentAttributes: nil)
        }

        closeButton.addTarget(self, action: #selector(doClose), for: .touchUpInside)
    }

    @objc
    func doClose() {
        dismiss(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
