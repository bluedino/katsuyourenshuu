//
//  KanaConverter.swift
//  KatsuyouRenshuu
//
//  Created by Burke, Matthew on 1/21/19.
//  Copyright © 2019-2020 BlueDino Software. All rights reserved.
//

import Foundation
import JavaScriptCore

public struct KanaConverter {
    let jsc: JSContext

    public init?() {
        guard let newJsc = JSContext() else {
            return nil
        }
        jsc = newJsc

        let filepath = Bundle.main.path(forResource: "wanakana", ofType: "js")!
        guard let script = try? String(contentsOfFile: filepath, encoding: .utf8) else {
            print("Error loading wanakana.js")
            return nil
        }
        guard let result = jsc.evaluateScript(script), result.toBool() == true else {
            print("Evaluating wanakana.js did not return true.")
            return nil
        }
    }

    public func toKana(_ str: String) -> String {
        guard let result = jsc.evaluateScript("wanakana.toKana('\(str)',{ IMEMode: true })") else { return "" }
        return result.toString()
    }

    public func isKana(_ str: String) -> Bool {
        guard let result = jsc.evaluateScript("wanakana.isKana('\(str)')") else { return false }
        return result.toBool()
    }
}
