//
//  AppDelegate.swift
//  KatsuyouRenshuu
//
//  Created by Burke, Matthew on 9/19/18.
//  Copyright © 2018-2020 BlueDino Software. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
}
