//
//  Quiz.swift
//  KatsuyouRenshuu
//
//  Created by Matthew Burke on 6/15/19.
//  Copyright © 2019-2020 BlueDino Software. All rights reserved.
//

import Foundation

public class Quiz {
    public enum Key {
        static let correct = "CORRECT"
        static let incorrect = "INCORRECT"
    }

    public enum Result {
        case correct
        case incorrect(answer: String)
    }

    private var words: [Verb] = []
    private var index = 0

    public private(set) var correct = 0
    public private(set) var incorrect = 0

    public var total: Int {
        return correct + incorrect
    }

    public var word: String {
        return words[index].text
    }

    // TODO: fix the !
    public private(set) var conjugation = Verb.conjugations.first!

    // TODO: change print statements to some sort of logging function
    private func loadWords(from filename: String) {
        guard let url = Bundle.main.url(forResource: filename, withExtension: "json") else {
            print("Could not get url for \(filename).json")
            return
        }

        guard let data = try? Data(contentsOf: url) else {
            print("Could not load data from \(filename).json")
            return
        }

        guard let newWords = try? JSONDecoder().decode([Verb].self, from: data) else {
            print("Could not parse JSON data from \(filename).json")
            return
        }

        words.append(contentsOf: newWords)
    }

    public func next() {
        index = Int.random(in: 0..<words.count)
        conjugation = Verb.conjugations.randomElement()!
    }

    public init(partOfSpeech: String = "verbs", correct: Int, incorrect: Int) {
        self.correct = correct
        self.incorrect = incorrect
        loadWords(from: partOfSpeech)
        next()
    }

    public func attempt(with candidate: String) -> Result {
        let answer = words[index][keyPath: conjugation.path]

        if answer == candidate {
            correct += 1
            return .correct
        } else {
            incorrect += 1
            return .incorrect(answer: answer)
        }
    }
}
