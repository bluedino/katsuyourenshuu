//
//  Constants.swift
//  KatsuyouRenshuu
//
//  Created by Burke, Matthew on 9/19/18.
//  Copyright © 2018-2020 BlueDino Software. All rights reserved.
//

import Foundation

let aSmall = "ぁ"
let iSmall = "ぃ"
let uSmall = "ぅ"
let eSmall = "ぇ"
let oSmall = "ぉ"

let a = "あ"
let i = "い"
let u = "う"
let e = "え"
let o = "お"

let ka = "か"
let ki = "き"
let ku = "く"
let ke = "け"
let ko = "こ"

let ga = "が"
let gi = "ぎ"
let gu = "ぐ"
let ge = "げ"
let go = "ご"

let sa = "さ"
let shi = "し"
let su = "す"
let se = "せ"
let so = "そ"

let za = "ざ"
let ji = "じ"
let zu = "ず"
let ze = "ぜ"
let zo = "ぞ"

let tsuSmall = "っ"

let ta = "た"
let chi = "ち"
let tsu = "つ"
let te = "て"
let to = "と"

let da = "だ"
let di = "ぢ"
let du = "づ"
let de = "で"
let `do` = "ど"

let na = "な"
let ni = "に"
let nu = "ぬ"
let ne = "ね"
let no = "の"

let ha = "は"
let hi = "ひ"
let hu = "ふ"
let he = "へ"
let ho = "ほ"

let ba = "ば"
let bi = "び"
let bu = "ぶ"
let be = "べ"
let bo = "ぼ"

let pa = "ぱ"
let pi = "ぴ"
let pu = "ぷ"
let pe = "ぺ"
let po = "ぽ"

let ma = "ま"
let mi = "み"
let mu = "む"
let me = "め"
let mo = "も"

let yaSmall = "ゃ"
let yuSmall = "ゅ"
let yoSmall = "ょ"

let ya = "や"
let yu = "ゆ"
let yo = "よ"

let ra = "ら"
let ri = "り"
let ru = "る"
let re = "れ"
let ro = "ろ"

let waSmall = "ゎ"

let wa = "わ"
let wo = "を"

let n = "ん"

let masen = ma + se + n
let masendeshita = masen + de + shi + ta
let mashita = ma + shi + ta
let mashou = ma + shi + yoSmall + u
let masu = ma + su
let nai = na + i
let rareru = ra + re + ru
let you = yo + u
