//
//  PartOfSpeech.swift
//  KatsuyouRenshuu
//
//  Created by Burke, Matthew on 1/21/19.
//  Copyright © 2019-2020 BlueDino Software. All rights reserved.
//

import Foundation

// TODO: make this a protocol?
/*

 protocol PartOfSpeach {
     var text: String { get }
     var conjugations: [Conjugation] { get }
 }



 */

struct PartOfSpeech: Codable {
    let text: String
    
}
