//
//  Adjectives.swift
//  KatsuyouRenshuu
//
//  Created by Burke, Matthew on 1/11/19.
//  Copyright © 2019-2020 BlueDino Software. All rights reserved.

import Foundation

enum AdjectiveType: String, Codable {
    case i
    case na
}

struct Adjective: Codable {
    let text: String
    let translation: String
    let type: AdjectiveType
}

extension Adjective {
    var teForm: String {
        // TODO: WRONG! FIXME
        return self.text
    }
}

