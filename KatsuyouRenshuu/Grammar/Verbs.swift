//
//  Verbs.swift
//  KatsuyouRenshuu
//
//  Created by Burke, Matthew on 9/19/18.
//  Copyright © 2018-2020 BlueDino Software. All rights reserved.
//

// See https://www.sljfaq.org/afaq/irregular-verbs.html for more info on irregular verbs

import Foundation

public typealias Conjugation = (name: String, path: KeyPath<Verb, String>)

public enum VerbType: Codable {
    public enum ExceptionalVerb: String, Codable {
        case suru
        case kuru
    }

    public enum VerbError: Error {
        case verbError  // TODO: find a better error to return
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let key = try container.decode(String.self)
        switch key {
        case "u":
            self = .u
        case "ru":
            self = .ru
        case "suru":
            self = .exception(verb: .suru)
        case "kuru":
            self = .exception(verb: .kuru)
        default:
            throw VerbError.verbError
        }
        
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        let key: String
        switch self {
        case .u:
            key = "u"
        case .ru:
            key = "ru"
        case .exception(verb: .suru):
            key = "suru"
        case .exception(verb: .kuru):
            key = "kuru"
        }
        try container.encode(key)
    }

    case ru
    case u
    case exception(verb: ExceptionalVerb)
}

// NOTE: modern Japanese has no verbs that end with hu, pu, yu
// TODO: what about zu? e.g. kinzu - to prohibit; what about dzu?
// TODO: decide how to properly handle errors?
public struct Verb: Codable {
    public let text: String
    public let translation: String
    public let type: VerbType

    public init(text: String, translation: String, type: VerbType) {
        self.text = text
        self.translation = translation
        self.type = type
    }

    // 連用形
    public var masuStem: String {
        switch type {
        case .ru:
            return String(text.dropLast())
        case .u:
            var text = self.text
            let lastSyllable = String(text.removeLast())
            let ending: String
            switch lastSyllable {
            case u:
                ending = i
            case ku:
                ending = ki
            case gu:
                ending = gi
            case su:
                ending = shi
            case tsu:
                ending = chi
            case nu:
                ending = ni
            case bu:
                ending = bi
            case mu:
                ending = mi
            case ru:
                ending = ri
            default:
                ending = "--"
            }
            return text + ending
        case .exception(let verb):
            switch verb {
            case .suru:
                return shi
            case .kuru:
                return ki
            }
        }
    }
}

extension Verb {
    public static var conjugations: [Conjugation] {
        return [
            // no need to quiz them on this one, it's the prompt: ("imperfective", \Verb.imperfective),
            ("conditional", \Verb.conditional),
            ("masu stem", \Verb.masuStem),
            ("negative", \Verb.negative),
            ("perfective", \Verb.perfective),
            ("potential", \Verb.potential),
            ("te form", \Verb.teForm),

            ("polite imperfective", \Verb.politeImperfective),
            ("polite negative", \Verb.politeNegative),
            ("polite negative perfective", \Verb.politeNegativePerfective),
            ("polite perfective", \Verb.politePerfective),
            ("polite volitional", \Verb.politeVolitional),
        ]
    }

    public var teForm: String {
        switch type {
        case .ru:
            return masuStem + te
        case .u:
            var text = self.text
            let lastSyllable = String(text.removeLast())
            let ending: String
            switch lastSyllable {
            case u, tsu, ru:
                // exception for aru ?
                // tou exception
                ending = tsuSmall + te
            case ku:
                // iku exception
                ending = i + te
            case gu:
                ending = i + de
            case su:
                ending = shi + te
            case nu, bu, mu:
                ending = n + de
            default:
                ending = "--"
            }
            return text + ending
        case .exception(let verb):
            switch verb {
            case .suru:
                return shi + te
            case .kuru:
                return ki + te
            }
        }
    }

    public var imperfective: String {
        return text
    }

    public var potential: String {
        // TODO: special cases
        // aru -- does not conjugate
        // wakaru -- wakareru

        switch type {
        case .ru:
            return masuStem + rareru
        case .u:
            var text = self.text
            let lastSyllable = String(text.removeLast())
            let transformed: String
            switch lastSyllable {
            case u:
                transformed = e
            case ku:
                transformed = ke
            case gu:
                transformed = ge
            case su:
                transformed = se
            case tsu:
                transformed = te
            case nu:
                transformed = ne
            case bu:
                transformed = be
            case mu:
                transformed = me
            case ru:
                transformed = re
            default:
                transformed = "--"
            }
            return text + transformed + ru
        case .exception(let verb):
            switch verb {
            case .suru:
                return "できる"
            case .kuru:
                return ko + rareru
            }
        }
    }

    public var conditional: String {
        // TODO: ~nai -> nakereba
        // contractions: nakya or nakucha
        switch type {
        case .ru:
            return masuStem + ba
        case .u:
            var text = self.text
            let lastSyllable = String(text.removeLast())
            let transformed: String
            switch lastSyllable {
            case u:
                transformed = e
            case ku:
                transformed = ke
            case gu:
                transformed = ge
            case su:
                transformed = se
            case tsu:
                transformed = te
            case nu:
                transformed = ne
            case bu:
                transformed = be
            case mu:
                transformed = me
            case ru:
                transformed = re
            default:
                transformed = "--"
            }
            return text + transformed + re + ba
        case .exception(let verb):
            switch verb {
            case .suru:
                return su + re + ba
            case .kuru:
                return ku + re + ba
            }
        }
    }

    public var negative: String {
        switch type {
        case .ru:
            return masuStem + nai
        case .u:
            var text = self.text
            let lastSyllable = String(text.removeLast())
            let transformed: String
            switch lastSyllable {
            case u:
                transformed = wa
            case ku:
                transformed = ka
            case gu:
                transformed = ga
            case su:
                transformed = sa
            case tsu:
                transformed = ta
            case nu:
                transformed = na
            case bu:
                transformed = ba
            case mu:
                transformed = ma
            case ru:
                transformed = ra
            default:
                transformed = "--"
            }
            return text + transformed + nai
        case .exception(let verb):
            switch verb {
            case .suru:
                return shi + nai
            case .kuru:
                return ko + nai
            }
        }
    }

    public var perfective: String {
        switch type {
        case .ru:
            return masuStem + ta
        case .u:
            var text = self.text
            let lastSyllable = String(text.removeLast())
            let ending: String
            switch lastSyllable {
            case u, tsu, ru:
                ending = tsuSmall + ta
            case ku: // deal with exception iku (to go)
                ending = i + ta
            case gu:
                ending = i + da
            case nu, bu, mu:
                ending = n + da
            case su:
                ending = shi + ta
            default:
                ending = "--"
            }
            return text + ending
        case .exception(let verb):
            switch verb {
            case .suru:
                return shi + ta
            case .kuru:
                return ki + ta
            }
        }
    }

    // TODO: verify this is correct
    public var volitional: String {
        // special cases
        if self.text == "ある" {
            return "あろう"
        }

        if self.text == "ない" {
            return "なかろう"
        }

        switch type {
        case .ru:
            return masuStem + you
        case .u:
            var text = self.text
            let lastSyllable = String(text.removeLast())
            let ending: String
            switch lastSyllable {
            case u:
                ending = o
            case tsu:
                ending = to
            case ru:
                ending = ro
            case ku:
                ending = ko
            case gu:
                ending = go
            case nu:
                ending = no
            case bu:
                ending = bo
            case mu:
                ending = mo
            case su:
                ending = su
            default:
                ending = "--"
            }
            return text + ending + u
        case .exception(verb: let verb):
            switch verb {
            case .suru:
                return shi + yo + u
            case .kuru:
                return ko + yo + u
            }
        }
    }

    public var politeImperfective: String {
        return masuStem + masu
    }

    public var politeNegative: String {
        return masuStem + masen
    }

    public var politeNegativePerfective: String {
        return masuStem + masendeshita
    }

    public var politePerfective: String {
        return masuStem + mashita
    }

    public var politeVolitional: String {
        return masuStem + mashou
    }

    // TODO: causative, passive, causative-passive, and ???
}


