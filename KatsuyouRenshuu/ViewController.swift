//
//  ViewController.swift
//  KatsuyouRenshuu
//
//  Created by Burke, Matthew on 9/19/18.
//  Copyright © 2018-2020 BlueDino Software. All rights reserved.
//

import AVFoundation
import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var correctLabel: UILabel!
    @IBOutlet weak var incorrectLabel: UILabel!
    @IBOutlet weak var correctAnswer: UILabel!

    private var maru: MaruView!
    private var batsu: BatsuView!

    private let infoSymbol: NSTextAttachment = {
        let a = NSTextAttachment()
        a.image = UIImage(systemName: "info.circle")?.withTintColor(.blue)
        return a
    }()

    // TODO: do we need to move this to viewWillAppear?
    //       if so, may need to make quiz optional... ugh
    
    private(set) var quiz: Quiz = {
        let correct = UserDefaults.standard.integer(forKey: Quiz.Key.correct)
        let incorrect = UserDefaults.standard.integer(forKey: Quiz.Key.incorrect)
        return Quiz(correct: correct, incorrect: incorrect)
    }()

    private let kanaConverter = KanaConverter()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(transmogrify(_:)),
                         name: UITextField.textDidChangeNotification,
                         object: nil)

        showCurrentWord()
        inputField.becomeFirstResponder()

        let width = view.bounds.width - 20
        let rect = CGRect(x: 10,
                          y: 10,
                          width: width,
                          height: width)

        maru = MaruView(frame: rect)
        maru.alpha = 0.0
        view.addSubview(maru)

        batsu = BatsuView(frame: rect)
        batsu.alpha = 0.0
        view.addSubview(batsu)

        let th = UITapGestureRecognizer(target: self,
                                        action: #selector(showConjugationDetails(_:)))
        questionLabel.isUserInteractionEnabled = true
        questionLabel.addGestureRecognizer(th)
    }

    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.set(quiz.correct, forKey: Quiz.Key.correct)
        UserDefaults.standard.set(quiz.incorrect, forKey: Quiz.Key.incorrect)

        NotificationCenter.default
            .removeObserver(self,
                            name: UITextField.textDidChangeNotification,
                            object: nil)

        super.viewWillDisappear(animated)
    }

    @objc
    func showConjugationDetails(_ sender: UITapGestureRecognizer) {
        guard sender.state == .ended,
              let vc = storyboard?.instantiateViewController(withIdentifier: ExplanationViewController.identifier) as? ExplanationViewController else {
                  return
              }

        vc.name = quiz.conjugation.name
        present(vc, animated: true, completion: nil)
    }

    @objc
    func transmogrify(_ obj: Any) {
        if let kc = kanaConverter,
           let input = inputField.text {
            let output = kc.toKana(input)
            inputField.text = output
        }
    }

    func showCurrentWord() {
        wordLabel.text = quiz.word

        let s = NSMutableAttributedString(string: "\(quiz.conjugation.name) ")
        s.append(NSAttributedString(attachment: infoSymbol))
        questionLabel.attributedText = s
    }

    func displayNewWord() {
        inputField.text = nil
        quiz.next()
        showCurrentWord()
    }

    // change this to the enter-key handler
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = inputField.text, !text.isEmpty else { return false }
        
        if let input = inputField.text {
            if input.hasSuffix("n") {
                inputField.text = "\(input)n"
                transmogrify([])
            }
        }

        guard let text = inputField.text, let kc = kanaConverter, kc.isKana(text) else { return false }

        if let input = inputField.text {
            let result = quiz.attempt(with: input)
            update(with: result)
            //displayNewWord()
        }
        return true
    }

    // improve the animation: https://medium.com/livefront/animating-font-size-in-uilabels-fb6fd273a5f3
    private func breathe(view: UIView) {
            UIView.addKeyframe(withRelativeStartTime: 0,
                               relativeDuration: 0.5,
                               animations: {
                                view.transform = CGAffineTransform(scaleX: 2.0, y: 3.0)
            })

            UIView.addKeyframe(withRelativeStartTime: 0.5,
                               relativeDuration: 0.5,
                               animations: {
                                view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
    }

    func update(with result: Quiz.Result) {
        switch result {
        case .correct:
            correctLabel.text = "Correct: \(quiz.correct)"
            SoundEffects.ding()
            UIView.animateKeyframes(withDuration: 2, delay: 0, options: [], animations: {
                self.breathe(view: self.correctLabel)

                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.4, animations: {
                    self.maru.alpha = 0.8
                    //self.maru.radius = 150
                })

                UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.6, animations: {
                    self.maru.alpha = 0.0
                    //self.maru.radius = 150
                })
            }, completion: { completed in
                self.displayNewWord()
            })
        case .incorrect(let answer):
            SoundEffects.buzz()
            incorrectLabel.text = "Incorrect: \(quiz.incorrect)"
            UIView.animateKeyframes(withDuration: 2, delay: 0, options: [], animations: {
                self.breathe(view: self.incorrectLabel)

                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.4, animations: {
                    self.batsu.alpha = 0.8
                    //self.maru.radius = 150
                })

                UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.6, animations: {
                    self.batsu.alpha = 0.0
                    //self.maru.radius = 150
                })

                UIView.addKeyframe(withRelativeStartTime: 0,
                                   relativeDuration: 0.4,
                                   animations: {
                                    self.correctAnswer.text = answer
                                    self.correctAnswer.alpha = 1
                                    self.inputField.alpha = 0
                })

                UIView.addKeyframe(withRelativeStartTime: 0.8,
                                   relativeDuration: 0.6,
                                   animations: {
                                    self.correctAnswer.alpha = 0
                                    self.inputField.alpha = 1
                })
            }, completion: { completed in
                self.displayNewWord()
            })
        }
    }
}

public class SoundEffects {
    static public func ding() {
        play(sound: "theDing", ext: "wav", hint: AVFileType.wav.rawValue)
    }

    static public func buzz() {
        play(sound: "buzz", ext: "mp3", hint: AVFileType.mp3.rawValue)
    }

    static var player: AVAudioPlayer?
    static private func play(sound: String, ext: String, hint: String) {
        guard let url = Bundle.main.url(forResource: sound, withExtension: ext) else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.ambient, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: hint)
            player?.play()
        } catch let error {
            print(error)
        }
    }}
