//
//  MaruView.swift
//  KatsuyouRenshuu
//
//  Created by Matthew Burke on 10/24/21.
//  Copyright © 2021 BlueDino. All rights reserved.
//

import UIKit

public class MaruView: UIView {
    public var radius: CGFloat = 80.0

    public override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }

    public required init?(coder: NSCoder) {
        fatalError("Init with coder not implemented.")
    }

    override public func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.setLineWidth(16.0)
        context.setStrokeColor(UIColor.green.cgColor)

        let center = CGPoint(x: rect.width/2, y: rect.height/2)
        context.addArc(center: center,
                       radius: radius,
                       startAngle: 0,
                       endAngle: 2*CGFloat.pi,
                       clockwise: true)
        context.strokePath()
    }
}
