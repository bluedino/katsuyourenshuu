//
//  BatsuView.swift
//  KatsuyouRenshuu
//
//  Created by Matthew Burke on 10/24/21.
//  Copyright © 2021 BlueDino. All rights reserved.
//

import UIKit

public class BatsuView: UIView {
    public var width: CGFloat = 80.0

    public override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }

    public required init?(coder: NSCoder) {
        fatalError("Init with coder not implemented.")
    }

    override public func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.setLineWidth(16.0)
        context.setStrokeColor(UIColor.red.cgColor)

        let fortyFiveDegrees = CGFloat.pi/4
        let center = CGPoint(x: rect.width/2, y: rect.height/2)
        let delta: CGFloat = width * cos(fortyFiveDegrees)

        context.move(to: CGPoint(x: center.x - delta, y: center.y - delta))
        context.addLine(to: CGPoint(x: center.x + delta, y: center.y + delta))

        context.move(to: CGPoint(x: center.x - delta, y: center.y + delta))
        context.addLine(to: CGPoint(x: center.x + delta, y: center.y - delta))

        context.strokePath()
    }
}
