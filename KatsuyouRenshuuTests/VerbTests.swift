//
//  VerbTests.swift
//  KatsuyouRenshuuTests
//
//  Created by Burke, Matthew on 9/19/18.
//  Copyright © 2018 BlueDino. All rights reserved.
//

import XCTest
@testable import KatsuyouRenshuu

class VerbTests: XCTest {
    let taberu = Verb(text: "たべる", translation: "to eat", type: .ru)
    let kaku = Verb(text: "かく", translation: "to write", type: .u)
    let asobu = Verb(text: "あそぶ", translation: "to play", type: .u)

    func testMasuStem() {
        XCTAssertEqual(taberu.masuStem, "たべ")

        XCTAssertEqual(kaku.masuStem, "かき")

        XCTAssertEqual(asobu.masuStem, "あそび")
    }
}
