
// Color blindness simulator: http://www.color-blindness.com/coblis-color-blindness-simulator/
// designing for color blind: https://www.getfeedback.com/resources/ux/how-to-design-for-color-blindness/

// TODO: color schemes for light/dark modes
// TODO: add explanation data
// TODO: color scheme on explanation view is bad

// TODO: "trend-meter"

// TODO: attributions for sound files (in about screen)

// TODO: save/restore correct/incorrect? or just overall trend

// TODO: lots of stats
// % correct for each conjugation...

// TODO: when correct answer is too long, it gets truncated... need to adjust font size

// TODO: insure sounds are not too loud or too quiet
// TODO: control to turn off/on sounds

// TODO: don't let user enter answer if it's not completely in hiragana
// TODO: don't let user enter if blank

// TODO: verify that all verbs are conjugating correctly
// note specifically osharu is wrong! and ???

// : sound - https://github.com/adamcichy/SwiftySound/blob/master/Sources/Sound.swift
//               https://stackoverflow.com/questions/32036146/how-to-play-a-sound-using-swift/47874593
//  xxx     correct sound - https://freesound.org/people/StavSounds/sounds/546081/
// https://freesound.org/people/collierhs_colinlib/sounds/588718/
//     incorrect sound - https://freesound.org/people/RICHERlandTV/sounds/216090/

